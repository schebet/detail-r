# detailer_app

A simple flutter app that displays a list of engineering interns and cohort details

### Screenshot
<div align=”center”>
  <img src="https://gitlab.com/schebet/detail-r/-/raw/master/assets/images/detailer.png/" width="250" />
</div>

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
