import 'package:flutter/material.dart';
import 'package:detailer_app/models/User.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var users = [
    User('Dorcas Cherono', "2021", "April", "DC"),
    User('Maxwell Kimaiyo', "2021", "April", "MK"),
    User('Gill Erick', "2021", "April", "GE"),
    User('Stacy Chebet', "2021", "April", "SC"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 140,
                child: Column(children: <Widget>[
                  const ListTile(
                      leading: Icon(
                        Icons.menu,
                      ),
                      trailing: Icon(
                        Icons.person,
                      )),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('Detail-R',
                            style: TextStyle(
                                fontFamily: 'Dancing Script',
                                fontWeight: FontWeight.bold,
                                fontSize: 35,
                                color: Colors.deepOrangeAccent)),
                      ])
                ]),
              ),
              Card(
                color: Colors.deepOrangeAccent,
                elevation: 1,
                borderOnForeground: false,
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                  ),
                ),
                child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: users.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ListTile(
                      hoverColor: Colors.white,
                      leading: CircleAvatar(
                        radius: 25,
                        backgroundColor: Colors.white,
                        foregroundColor: Colors.deepOrangeAccent,
                        child: Text('${users[index].acronym}',
                            style: TextStyle(
                                fontFamily: 'Dancing Script',
                                fontWeight: FontWeight.bold,
                                color: Colors.deepOrangeAccent)),
                      ),
                      title: Text('${users[index].name}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Month:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text: ' ${users[index].month}',
                                      style: TextStyle(color: Colors.black)),
                                ],
                              ),
                            ),
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Year:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text: ' ${users[index].year}',
                                      style: TextStyle(color: Colors.black)),
                                ],
                              ),
                            ),
                          ]),
                      isThreeLine: true,
                      trailing: Icon(
                        Icons.timeline,
                        color: Colors.deepOrangeAccent,
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(
                      thickness: 3,
                      color: Colors.white,
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
