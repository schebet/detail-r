import 'package:detailer_app/screens/home.dart';
import 'package:detailer_app/screens/screen_wrapper.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (context) => ScreenWrapper());

  }
}
